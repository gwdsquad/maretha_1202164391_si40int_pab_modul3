package com.example.maretha_1202164391_si40int_pab_modul3;

public class ItemObject {
    public String mNama, mKeterangan, mGender;
    public int mGambar;
    public ItemObject(String mNama, String mKeterangan, int mGambar, String mGender){
        this.mNama = mNama;
        this.mKeterangan = mKeterangan;
        this.mGambar = mGambar;
        this.mGender = mGender;
    }

}

package com.example.maretha_1202164391_si40int_pab_modul3;

import android.content.DialogInterface;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    AlertDialog.Builder dialog;
    String nama, keterangan, gender;
    ArrayList<ItemObject> Pengguna = new ArrayList<>();
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImage = new ArrayList<>();
    RecyclerView recycle;
    RecycleViewAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.customdialog, null);

                final EditText mName = mView.findViewById(R.id.text_name);
                final EditText mJob = mView.findViewById(R.id.text_position);
                final RadioGroup mGender = mView.findViewById(R.id.genderbutton);
                Button buttonAdd = mView.findViewById(R.id.button_add);
                RadioButton maleBox = mView.findViewById(R.id.button_male);
                RadioButton femaleBox = mView.findViewById(R.id.button_female);

                //01
                buttonAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!mName.getText().toString().isEmpty() && !mJob.getText().toString().isEmpty()) {
                            Toast.makeText(MainActivity.this, "Added to List", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                        if(mGender.equals(R.id.button_male)){
                            Pengguna.add(new ItemObject(mName.getText().toString(), mJob.getText().toString(), R.drawable.laki, mGender.toString()));
                        }
                        if(mGender.equals(R.id.button_female)){
                            Pengguna.add(new ItemObject(mName.getText().toString(), mJob.getText().toString(), R.drawable.wanita, mGender.toString()));
                        }

                    }
                });
                mBuilder.setView(mView);
                AlertDialog dialog = mBuilder.create();
                dialog.show();


            }
        });
    }
}



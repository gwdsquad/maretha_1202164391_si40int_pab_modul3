package com.example.maretha_1202164391_si40int_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerViewAccessibilityDelegate;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ViewTarget;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.HolderItem> {

    Context context;
    List<ItemObject> itemObjects;

 //   public RecyclerViewAdapter(Context context, List<ItemObject> itemObjects){
   //     this.context = context;
     //   this.itemObjects = itemObjects;
    // }

    @NonNull
    @Override
    public HolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item, null);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderItem holder, int position) {
        holder.nama.setText(itemObjects.get(position).mNama);
        holder.keterangan.setText(itemObjects.get(position).mKeterangan);
        Glide.with(context).load(itemObjects.get(position).mGambar)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return itemObjects.size();
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        public TextView nama, keterangan;
        public ImageView image;

        public HolderItem(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama);
            keterangan = itemView.findViewById(R.id.keterangan);
            image = itemView.findViewById(R.id.imagelaki);
        }
    }

}
